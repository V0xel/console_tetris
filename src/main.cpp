#include <cstring>
#include <cstdio>
#include <bitset>
#include <windows.h>
#include <random>
#include <algorithm>

#define ARRAY_COUNT(arr) (sizeof(arr) / sizeof(arr[0]))
#define IS_DOWN(x)( GetAsyncKeyState(x) & 0x8000 )

enum BlockType
{
	I,
	O,
	T,
	J,
	L,
	S,
	Z,
	TYPES_COUNT
};

enum TetroRotate
{
	ANGLE_0,
	ANGLE_90,
	ANGLE_180,
	ANGLE_270,
	ANGLES_COUNT
};

enum Colors
{
	BLACK,
	BLUE,
	GREEN,
	CYAN,
	RED,
	MAGENTA,
	BROWN,
	LIGHTGRAY,
	DARKGRAY,
	LIGHTBLUE,
	LIGHTGREEN,
	LIGHTCYAN,
	LIGHTRED,
	LIGHTMAGENTA,
	YELLOW,
	WHITE
};

struct Tetromino
{
	char block[16]; // 4x4 tetromino
};

struct GameField
{
	int width;
	int height;
	unsigned char* data;
};

int getIndex(int x, int y, TetroRotate r = ANGLE_0)
{
	int count = 4; // Num of elements in row

	switch (r)
	{
	case ANGLE_0:
		return y * count + x;
	case ANGLE_90:
		return count * (count - 1) + y - count * x;
	case ANGLE_180:
		return count * count - 1 - count * y - x;
	case ANGLE_270:
		return count - 1 - y + count * x;
	default:
		break;
	}
	return 0;
}
struct ScreenBuffer
{
	int width;
	int height;
	char* charData;
	WORD* attrData;
	HANDLE consoleHandle;
	DWORD writtenOutBytes;
	DWORD writtenOutAttributes;
};

struct FixedPosition
{
	int x;
	int y;
};

struct DynamicPosition
{
	float x;
	float y;
};


enum KeyInputs
{
	KEY_LEFT,
	KEY_RIGHT,
	KEY_DOWN,
	KEY_Q
};

struct ActiveBlock
{
	FixedPosition pos;
	TetroRotate angle;
	BlockType type;
	DynamicPosition dynPos;
};

bool checkColision(FixedPosition pos, TetroRotate rot, BlockType type, GameField& field, Tetromino* blocks)
{
	for (int yb = 0; yb < 4; yb++)
	{
		for (int xb = 0; xb < 4; xb++)
		{
			int index = getIndex(xb, yb, rot);
			int fieldIndex = (yb + pos.y)*field.width + (xb + pos.x);

			if (xb + pos.x >= 0 && xb + pos.x < field.width &&
				yb + pos.y >= 0 && yb + pos.y < field.height)
			{
				if (blocks[type].block[index] != '.' && field.data[fieldIndex] != 0)
					return true;
			}
		}
	}
	return false;
}

int randomInt(const int& begin, const int& end, std::mt19937& rng)
{
	const std::uniform_int_distribution<int>distribution(begin, end);
	return distribution(rng);
}

int main()
{
	// Create blocks types
	Tetromino blocks[TYPES_COUNT];
	memcpy(blocks[I].block, { "..X...X...X...X." }, sizeof(blocks[I].block));
	memcpy(blocks[O].block, { ".XX..XX........." }, sizeof(blocks[O].block));
	memcpy(blocks[T].block, { "..X..XX...X....." }, sizeof(blocks[T].block));
	memcpy(blocks[J].block, { "..X...X..XX....." }, sizeof(blocks[J].block));
	memcpy(blocks[L].block, { ".X...X...XX....." }, sizeof(blocks[L].block));
	memcpy(blocks[S].block, { ".X...XX...X....." }, sizeof(blocks[S].block));
	memcpy(blocks[Z].block, { "..X..XX..X......" }, sizeof(blocks[Z].block));

	// Create background field
	GameField backField = {};
	backField.width = 14;
	backField.height = 19;
	backField.data = (unsigned char*)calloc(backField.width * backField.height, sizeof(unsigned char));
	for (int y = 0; y < backField.height; y++)
		for (int x = 0; x < backField.width; x++)
			backField.data[y*backField.width + x] = (x == 0 || x == backField.width - 1 || y == backField.height - 1) ? TYPES_COUNT : 0;

	// Create screen buffer
	ScreenBuffer buffer = {};
	buffer.width = 120;
	buffer.height = 60;
	buffer.charData = (char*)calloc(buffer.width*buffer.height, sizeof(unsigned char));
	buffer.consoleHandle = CreateConsoleScreenBuffer(GENERIC_READ | GENERIC_WRITE, 0, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
	SetConsoleActiveScreenBuffer(buffer.consoleHandle);
	buffer.attrData = (WORD*)calloc(buffer.width*buffer.height, sizeof(WORD));
	//TODO: For now, after pixel font it will be redundant
	for (int i = 0; i < 16; i++)
		buffer.attrData[i] = WHITE | BLACK << 4;
	// Disable cursor
	CONSOLE_CURSOR_INFO info;
	info.dwSize = 100;
	info.bVisible = FALSE;
	SetConsoleCursorInfo(buffer.consoleHandle, &info);

	// Game state variables
	bool isRotateHold = true;
	std::bitset<4>inputs = {};
	std::mt19937 rng{ std::random_device{}() };
	int lines[4] = {};
	int addedLines = 0;
	int score = 0;
	int lineBonus = 1;
	int activeCount = 1;
	int inputDelayMs = 64;
	bool isResumedFromAnimation = true;
	float fallSpeed = 0.0025;
	float moveSpeed = 0.02;
	float moveSpeedY = 0.08;
	int speedStepsY = 5;
	float stepSpeedY = 0;
	// Active block variables
	ActiveBlock activeBlock = {};
	activeBlock.pos.x = backField.width / 2 - 2;
	activeBlock.dynPos.x = activeBlock.pos.x;
	//activeBlock.type = (BlockType)randomInt(I, Z, rng);
	activeBlock.type = O;
	TetroRotate tempAngle = {};
	DynamicPosition tempPos = {};
	// Gameplay timers
	LARGE_INTEGER dTime = {};
	LARGE_INTEGER lineTime = {};
	// Game time governing variables
	LARGE_INTEGER StartingTime{}, EndingTime{}, ElapsedMs{};
	LARGE_INTEGER Frequency{};
	QueryPerformanceFrequency(&Frequency);

	// Game loop
	for (;;)
	{
		QueryPerformanceCounter(&StartingTime);

		// ==================================================================INPUT================================================================================

		// Check input, mapping to inputs bitset in-place with small buffering
		for (int k = 0; k < 4; k++)
			inputs[k] = IS_DOWN((unsigned char)("\x25\x27\x28\x51"[k])) != 0;
		//TODO: Improve input precision/feeling -- hard to move by one, also we want to hold key to move several "sqaures"
		tempPos = activeBlock.dynPos;
		tempAngle = activeBlock.angle;
		tempPos.x += inputs[KEY_RIGHT] ? moveSpeed * dTime.QuadPart : 0;
		tempPos.x -= inputs[KEY_LEFT] ? moveSpeed * dTime.QuadPart : 0;
		// Because speed in Y direction can be super fast and skip collision we have to divide it to steps
		stepSpeedY = inputs[KEY_DOWN] ? (moveSpeedY * dTime.QuadPart) / speedStepsY : 0;

		if (inputs[KEY_Q])
		{
			if (isRotateHold)
			{
				tempAngle = (TetroRotate)((activeBlock.angle + 1) % 4);
			}
			isRotateHold = false;
		}
		else
			isRotateHold = true;

		// ==================================================================SIMULATION================================================================================

		// Input to movement simulation
		//TODO: Add ability to insta placing of active block
		FixedPosition tempPosFixed = { (int)tempPos.x, (int)tempPos.y };
		// Checking collision in steps in order to prevent skips in detection for higher Y speeds - especially for "I" block
		for (int i = 0; i < speedStepsY; i++)
		{
			tempPos.y = activeBlock.dynPos.y + (stepSpeedY*i);
			tempPosFixed.y = (int)tempPos.y;
			if (checkColision(tempPosFixed, tempAngle, activeBlock.type, backField, blocks))
			{
				break;
			}
			else
			{
				activeBlock.pos = tempPosFixed;
				activeBlock.angle = tempAngle;
				activeBlock.dynPos = tempPos;
			}
		}

		// Gameplay logic simulation
		//TODO: Merge it with upper loop and remove second collison check -> Cant trivially cause we dont distiguish collison type
		tempPos.y += fallSpeed * (float)dTime.QuadPart;
		tempPos.x = activeBlock.dynPos.x;

		// Increase falling speed every 30 blocks
		if (activeCount % 30 == 0)
		{
			fallSpeed += 0.00002;
			lineBonus += 2;
		}

		// Move blocks from above the particular line down	 and set removed to 0
		if (!isResumedFromAnimation)
		{
			lineTime.QuadPart += dTime.QuadPart;
			// Update when 'animation' finished (>=250ms)
			if (lineTime.QuadPart >= 250)
			{
				for (int i = 0; i < addedLines; ++i)
				{
					for (int y = lines[i]; y > 0; y--)
					{
						for (int x = 1; x < backField.width - 1; x++)
						{
							backField.data[y * backField.width + x] = backField.data[(y - 1) * backField.width + x];
							backField.data[x] = 0;
						}
					}
				}
				addedLines = 0;
				isResumedFromAnimation = true;
				lineTime.QuadPart = 0;
			}
		}

		tempPosFixed.x = (int)tempPos.x;
		tempPosFixed.y = (int)tempPos.y;

		if (!checkColision(tempPosFixed, activeBlock.angle, activeBlock.type, backField, blocks))
		{
			activeBlock.pos = tempPosFixed;
			activeBlock.angle = activeBlock.angle;
			activeBlock.dynPos = tempPos;
		}
		else
		{
			// Place piece on the board
			for (int yb = 0; yb < 4; yb++)
			{
				for (int xb = 0; xb < 4; xb++)
				{
					int index = getIndex(xb, yb, activeBlock.angle);
					int backIt = (yb + activeBlock.pos.y)*backField.width + xb + activeBlock.pos.x;
					if (blocks[activeBlock.type].block[index] != '.')
					{
						backField.data[backIt] = 'X';
					}
				}
			}

			// Check for lines, increase score and flag lines to remove them in next gameplay logic iteration after the animation is fully displayed
			if (isResumedFromAnimation)
			{
				for (int yb = 0; yb < 4; yb++)
				{
					if (activeBlock.pos.y + yb < backField.height - 1)
					{
						bool isLine = true;
						for (int x = 1; x < backField.width - 1; x++)
							isLine &= (backField.data[(activeBlock.pos.y + yb) * backField.width + x]) != 0;

						if (isLine)
						{
							isResumedFromAnimation = false;
							addedLines++;
							score += lineBonus;
							// Set lines symbol
							for (int x = 1; x < backField.width - 1; x++)
							{
								backField.data[(activeBlock.pos.y + yb) * backField.width + x] = TYPES_COUNT + 1;
							}
							lines[addedLines - 1] = activeBlock.pos.y + yb;
						}
					}
				}
			}

			// Generate a new piece
			activeBlock.pos.x = backField.width / 2 - 2;
			activeBlock.pos.y = 0;
			activeBlock.dynPos.x = activeBlock.pos.x;
			activeBlock.dynPos.y = 0.0f;
			activeBlock.angle = ANGLE_0;
			tempAngle = ANGLE_0; //! this feels wrong
			//activeBlock.type = (BlockType)randomInt(I, Z, rng);
			activeBlock.type = O;

			// Check for lose
			if (checkColision(activeBlock.pos, activeBlock.angle, activeBlock.type, backField, blocks))
			{
				break;
			}
			activeCount++;
		}

		// ==================================================================RENDERING================================================================================
		//TODO: Different colors for pieces
		// Drawing game field
		int offsetX = 16;
		int offsetY = 2;
		for (size_t y = 0; y < backField.height; y++)
		{
			for (size_t x = 0; x < backField.width; x++)
			{
				// Each position printed two times in X (to make it more square looking)
				for (size_t i = 0; i < 2; i++)
				{
					// Extend (+ i + x) is actually x * (extend - 1 ) ( for 2 its (2 - 1) which is 1)
					int bufferIt = ((y + offsetY)*buffer.width + (x + offsetX)) + x + i;  // +i + x
					buffer.charData[bufferIt] = 219;

					if (backField.data[y*backField.width + x] == TYPES_COUNT)
					{
						buffer.attrData[bufferIt] = GREEN | GREEN << 4;
					}
					else if (backField.data[y*backField.width + x] == 'X')
					{
						buffer.attrData[bufferIt] = RED | RED << 4;
					}
					else if (backField.data[y*backField.width + x] == TYPES_COUNT + 1)
					{
						buffer.attrData[bufferIt] = WHITE | WHITE << 4;
					}
					else
					{
						buffer.attrData[bufferIt] = BROWN | BROWN << 4;
					}
				}
			}
		}

		// Drawing active block
		for (int yb = 0; yb < 4; yb++)
		{
			for (int xb = 0; xb < 4; xb++)
			{
				for (size_t i = 0; i < 2; i++)
				{
					int index = getIndex(xb, yb, activeBlock.angle);
					int bufferIt = (yb + activeBlock.pos.y + offsetY)*buffer.width + (xb + activeBlock.pos.x + offsetX) + activeBlock.pos.x + xb + i;
					if (blocks[activeBlock.type].block[index] != '.')
					{
						buffer.attrData[bufferIt] = YELLOW | YELLOW << 4;
					}
				}
			}
		}

		// TODO: "Pixel" printing font
		sprintf_s(&buffer.charData[0], 16, "SCORE: %8d", score);

		// Push buffers
		WriteConsoleOutputCharacterA(buffer.consoleHandle, buffer.charData, buffer.height * buffer.width, { 0,0 }, &buffer.writtenOutBytes);
		WriteConsoleOutputAttribute(buffer.consoleHandle, buffer.attrData, buffer.height * buffer.width, { 0, 0 }, &buffer.writtenOutAttributes);

		// Main time managment
		QueryPerformanceCounter(&EndingTime);
		ElapsedMs.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;
		ElapsedMs.QuadPart *= 1000;
		ElapsedMs.QuadPart /= Frequency.QuadPart;
		dTime.QuadPart = ElapsedMs.QuadPart;
		if (dTime.QuadPart < 16)
		{
			Sleep(16 - dTime.QuadPart);
			dTime.QuadPart = 16;
		}
		else
		{
			dTime.QuadPart = std::clamp((uint64_t)dTime.QuadPart, (uint64_t)0, (uint64_t)50);
		}

		//? Clear buffers, DO I NEED THAT???
		//memset(buffer.charData, 0, sizeof(buffer.charData));
		//memset(buffer.attrData, 0, sizeof(buffer.charData));
	}
}